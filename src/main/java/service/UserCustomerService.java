package service;


import dto.CustomerRequest;
import models.Customer;
import template.Template;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class UserCustomerService {

    @Transactional
    public Template post(CustomerRequest customerRequest) {
        System.out.println("Service");

        Customer customer = new Customer();
        //set data baru untuk useradmin
        customer.setUsername(customerRequest.getUsername());
        customer.setPassword(customerRequest.getPassword());
        customer.setEmail(customerRequest.getEmail());
        customer.setNamaLengkap(customerRequest.getNamaLengkap());
        System.out.println("End Setting User");
        //save data user admin
        Customer.persist(customer);
        System.out.println("save user");

        //return template response
        return new Template(true, "succes", customer);
    }
}