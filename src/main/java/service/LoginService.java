package service;



import dto.LoginRequest;
import models.UserAdmin;
import template.Template;
import util.JwtUtil;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LoginService {
    public Template login(LoginRequest loginRequest) {
        UserAdmin byUserAdmin = UserAdmin.findbyUsername(loginRequest.getUsername());
        if (byUserAdmin == null) {
            return new Template(false, "gagal login username salah", null);
        }
        if (!loginRequest.getPassword().equals(byUserAdmin.getPassword())) {
            return new Template(false, "gagal login passord admin salah", null);
        }
        //generate token
        String token = JwtUtil.generateToken(byUserAdmin);
        return new Template(true, "succes login", token);
    }

}
//example postman
// {
//     "username":"admin",
//     "password":"admin"
// }
