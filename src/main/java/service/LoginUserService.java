package service;


import dto.LoginUserRequest;
import models.Customer;
import template.Template;
import util.JwtUtil;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class LoginUserService {
    public Template login(LoginUserRequest LoginUserRequest) {
        Customer byCustomer = Customer.findbyUsername((LoginUserRequest.getUsername()));
        if (byCustomer == null) {
            return new Template(false, "gagal login", false);
        }
        if (!LoginUserRequest.getPassword().equals(byCustomer.getPassword())) {
            return new Template(false, "gagal login pasword salah", false);
        }
        //generate token
        String token = JwtUtil.generateTokenCus(byCustomer);
        return new Template(true, "succes login", token);
    }


}