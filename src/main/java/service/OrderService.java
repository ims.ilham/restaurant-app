package service;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
@ApplicationScoped
public class OrderService implements IAction {


    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class.getName());

    @Inject
    EntityManager em;

    @Override
    public SimpleResponse insert(Object param, String header) {
        return null;
    }

    @Override
    public SimpleResponse update(Object param, String header) {
        return null;
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        return null;
    }

    @Override
    public SimpleResponse entity(Object param) {
        return null;
    }

    public SimpleResponse delete(Object param){
        return null;
    }
}
