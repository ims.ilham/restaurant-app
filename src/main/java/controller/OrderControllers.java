package controller;

import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import service.MenuService;
import service.OrderService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/master/order")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrderControllers implements IAction {


    @Inject
    OrderService service;
    @Override
    public SimpleResponse insert(Object param, @HeaderParam("X-Consumer-Custom-ID") String header) {
        return service.insert(param,header);
    }

    @Override
    public SimpleResponse update(Object param,@HeaderParam("X-Consumer-Custom-ID") String header) {
        return service.update(param,header);
    }

    @Override
    public SimpleResponse inquiry(Object param) {
        return service.inquiry(param);
    }

    @Override
    public SimpleResponse entity(Object param) {
        return service.entity(param);
    }

    public SimpleResponse delete(Object param){
        return service.delete(param);
    }
}
