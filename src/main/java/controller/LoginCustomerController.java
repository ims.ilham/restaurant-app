package controller;

import dto.LoginUserRequest;
import org.eclipse.microprofile.openapi.annotations.Operation;
import service.LoginUserService;
import template.Template;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/auth/user/login")
public class LoginCustomerController {
    @Inject
    LoginUserService loginUserService;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Login", description = "Silahkan masukan username dan password")
    public Template login(LoginUserRequest loginUserRequest) {

        Template response = loginUserService.login(loginUserRequest);
        return new Template(true, "Berhasil Masuk", "");
    }
}
