package controller;


import com.barrans.util.IAction;
import com.barrans.util.SimpleResponse;
import service.CustomerService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/master/customer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerControllers implements IAction {
    @Inject
    CustomerService customerService;

    @Override
    public SimpleResponse insert(Object o, String s) {
        return null;
    }

    @Override
    public SimpleResponse update(Object o, String s) {
        return null;
    }

    @Override
    public SimpleResponse inquiry(Object o) {
        return null;
    }

    @Override
    public SimpleResponse entity(Object o) {
        return null;
    }
}
