package controller;

import dto.CustomerRequest;
import lombok.extern.slf4j.Slf4j;

import org.eclipse.microprofile.openapi.annotations.Operation;
import service.UserCustomerService;
import template.Template;
import util.RegexUtil;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/master/customer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Slf4j
public class UserCustomerController {
    @Inject
    UserCustomerService userCustomerService;
    @POST
    @Path("/createUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add User", description = "Silahkan masukan data user sesuai format")
    public Template addCustomer(CustomerRequest customerRequest) {

        //validasi user
        Template responseValidasi = validasiCustomerRequest(customerRequest);
        if (responseValidasi != null) {
            return responseValidasi;
        }
        //generate password
        String password = RegexUtil.generatePassword();
        customerRequest.setPassword(password);
        //save user admin

        //masuk ke user admin service
        Template response = userCustomerService.post(customerRequest);
        return response;
    }
    public static Template validasiCustomerRequest(CustomerRequest customerRequest) {
        Boolean namaLengkap = RegexUtil.isAlphaOnly(customerRequest.getNamaLengkap());
        if (!namaLengkap) {
            return new Template(false, "Nama Lengkap Harus Alphabeth", customerRequest.getNamaLengkap());
        }
        Boolean email = RegexUtil.isEmail(customerRequest.getEmail());
        if(!email){
            return new Template(false, "Email harus sesuai format", customerRequest.getEmail());
        }
        //pasword harus lebih dari 8 karakter
        Boolean password = RegexUtil.isPassword(customerRequest.getPassword());
        if(!password){
            return new Template(false, "Password harus lebih dari 8 karakter", customerRequest.getPassword());
        }
        return null;
    }


}
