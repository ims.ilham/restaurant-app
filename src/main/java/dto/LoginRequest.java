package dto;

import lombok.Setter;
import lombok.Getter;
@Setter
@Getter
public class LoginRequest {
    private String username;
    private String password;
}
