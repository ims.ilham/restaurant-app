package dto;

import lombok.Getter;

@Getter
public class CustomerRequest {

    private String username;
    private String password;
    private String namaLengkap;
    private String email;


    public void setPassword(String password) {

        this.password = password;

    }
}

