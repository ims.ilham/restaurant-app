package dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginCustomerRequest {
    private String username;
    private String password;

}
