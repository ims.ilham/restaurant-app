package models;


import com.barrans.util.CommonObjectActiveAndCreatedDate;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Entity
@Table(name = "orderdetail")
public class OrderDetail extends CommonObjectActiveAndCreatedDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "menu_id")
    public Menu menu;

    @NotNull
    @Column(name = "jumlah")
    public Integer quantity;

    //order_detail_id
    @Column(name = "order_detail_id")
    public Integer orderDetailId;



}
