package models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
@Table(name ="chef")
public class Chef extends CommonObjectActiveAndCreatedDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "username")
    @NotNull(message = "Please insert username")
    public String username;

    @Column(name = "numberPhone")
    @NotNull(message = "Please insert numberPhone")
    public String numberPhone;

    @Column(name = "password")
    @NotNull(message = "Please insert password")
    public String password;

    //Chef id
    @Column(name = "chef_id")
    private Long chef_id;

}
