package models;


import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name ="Customer")
@Setter
@Getter
public class Customer  extends PanacheEntityBase {

    @Id
    @SequenceGenerator(name = "customerSequence",
            sequenceName = "customer_id_sequence", allocationSize = 1, initialValue = 1)

    @Column(name = "nama_lengkap")
    @NotBlank(message = "nama tidak boleh kosong")
    private String namaLengkap;

    @Column(name = "username")
    @NotBlank(message = "username tidak boleh kosong")
    private String username;

    @Column(name = "password")
    @NotBlank(message = "password tidak boleh kosong")
    private String password;

    @Column(name = "email")
    @NotBlank(message = "alamat email tidak boleh kosong")
    private String email;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "update_at")
    private LocalDateTime updateAt;

    //customer_id
    @Column(name = "customer_id")
    private Long customer_id;
    //one to many order

    //order_id
    @Column(name = "order_id")
    private Long order_id;

    //payment_id
    @Column(name = "payment_id")
    private Long payment_id;



    public static Customer findbyUsername(String username) {

        return find("username", username).firstResult();
    }

}