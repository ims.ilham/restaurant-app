package models;


import com.barrans.util.CommonObjectCreatedDate;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
@Entity
@Table(name = "order")
public class Order extends CommonObjectCreatedDate implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    public Customer customer;

    @NotNull
    @Column(name = "total")
    public Integer total;

    @NotNull
    @Column(name = "kembalian")
    public Integer kembalian;

    @NotNull
    @Column(name = "idtransaksi")
    public String idTransaksi;

    //List Chef
    @ManyToOne
    @JoinColumn(name = "chef_id")
    public Chef chef;

    //Payment
    @OneToMany
    @JoinColumn(name = "payment_id")
    public List<Payment> payment;


    //Order Detail
    @OneToMany
    @JoinColumn(name = "orderdetail_id")
    public List<OrderDetail> orderDetail;


}
