package models;

import com.barrans.util.CommonObjectCreatedDate;
import io.smallrye.common.constraint.NotNull;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "payment")
public class Payment extends CommonObjectCreatedDate implements Serializable {


    private static final long serialVersionUID = 1L;

    @Column(name = "paymentType")
    public String paymentType;

    @Column(name = "paymentStatus")
    public String paymentStatus;

    //one to many customer
    @ManyToOne
    @JoinColumn(name = "customer_id")
    public Customer customer;


}
