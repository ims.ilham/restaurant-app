package models;

import com.barrans.util.CommonObjectActiveAndCreatedDate;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "Menu")
public class Menu extends CommonObjectActiveAndCreatedDate implements Serializable {

//    @OneToMany(mappedBy = "menu")
//    private List<FoodItem> foodItems;

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "nama")
    public String nama;

    @NotNull
    public Double harga;

    @ManyToOne
    @JoinColumn(name = "unit_id")
    public Unit unit;
}
