package util;

import io.smallrye.jwt.build.Jwt;
import models.Customer;
import models.UserAdmin;

import java.time.Duration;
import java.util.Date;


public class JwtUtil {

    public static String generateToken(UserAdmin userAdmin) {
        String token = Jwt.issuer("xxxxx")
                .issuedAt(new Date().toInstant())
                .subject(userAdmin.getUsername())
                .groups("admin")
                .expiresIn(Duration.ofHours(1))
                .claim("username", userAdmin.getUsername())
                .claim("id", userAdmin.getId())
                .sign();
        return token;

    }

    public static String generateTokenCus(Customer customer) {
        String token = Jwt.issuer("xxxxx")
                .issuedAt(new Date().toInstant())
                .subject(customer.getUsername())
                .groups("customer")
                .expiresIn(Duration.ofHours(1))
                .claim("username", customer.getUsername())
                .claim("id", customer.getCustomer_id())
                .sign();
        return token;
    }
}
